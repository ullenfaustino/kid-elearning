package org.android.utilities;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

public class SoundPlayer {

	Context mContext;

	public SoundPlayer(Context ctx) {
		this.mContext = ctx;
	}

	public void playSound(final String filename, final Boolean isRepeat) {
		final Handler mHandler = new Handler();
		new Thread(new Runnable() {

			@Override
			public void run() {
				mHandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						try {
							MediaPlayer m = new MediaPlayer();
							if (m.isPlaying()) {
								m.stop();
								m.release();
								m = new MediaPlayer();
							}
							AssetFileDescriptor descriptor = mContext
									.getAssets().openFd(filename);
							m.setDataSource(descriptor.getFileDescriptor(),
									descriptor.getStartOffset(),
									descriptor.getLength());
							descriptor.close();
							m.prepare();
							m.setVolume(1f, 1f);
							m.setLooping(isRepeat);
							m.start();
						} catch (Exception e) {
							Log.d("TEST", "ERROR: " + e.toString());
						}
					}
				}, 250);
			}
		}).start();
	}
	
//	public void StopPlayingSound() {
//		if (m.isPlaying()) {
//			m.stop();
//			m.release();
//			m = null;
//		}
//	}

}
