package org.android.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class HighScoreDB extends SQLiteOpenHelper {

	protected static String DBname = "high_scores";
	protected static int SCHEMA_VERSION = 1;
	protected static String TB_letter = "letter";
	protected static String TB_number = "number";
	protected static String TB_color = "color";

	public HighScoreDB(Context context) {
		super(context, DBname, null, SCHEMA_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_letter
					+ " (high_score INTEGER, name TEXT)");
			db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_number
					+ " (high_score INTEGER, name TEXT)");
			db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_color
					+ " (high_score INTEGER, name TEXT)");
		} catch (Exception e) {
			Log.d("TEST",
					"****ERROR ON CREATING DATABASE FOR ABOUT****\n\t===>"
							+ e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d("TEST", "****UPDGRADE DATABASE REQUIRED*****");
		db.execSQL("DROP TABLE IF EXISTS " + TB_letter);
		db.execSQL("DROP TABLE IF EXISTS " + TB_number);
		db.execSQL("DROP TABLE IF EXISTS " + TB_color);
		this.onCreate(db);
	}

	public void saveColorToDB(int highscore, String name) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("high_score", highscore);
			cv.put("name", name);
			getWritableDatabase().insertOrThrow(TB_color, "high_score", cv);
			getWritableDatabase().close();
			Log.d("TEST", "****SUCCESSFULLY SAVE TO DB****\n\t===>" + highscore);
		} catch (Exception e) {
			Log.d("TEST", "****ERROR ON saveToDB****\n\t===>" + e.toString());
			e.printStackTrace();
		}
	}

	public void saveLetterToDB(int highscore, String name) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("high_score", highscore);
			cv.put("name", name);
			getWritableDatabase().insertOrThrow(TB_letter, "high_score", cv);
			getWritableDatabase().close();
			Log.d("TEST", "****SUCCESSFULLY SAVE TO DB****\n\t===>" + highscore);
		} catch (Exception e) {
			Log.d("TEST", "****ERROR ON saveToDB****\n\t===>" + e.toString());
			e.printStackTrace();
		}
	}

	public void saveNumberToDB(int highscore, String name) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("high_score", highscore);
			cv.put("name", name);
			getWritableDatabase().insertOrThrow(TB_number, "high_score", cv);
			getWritableDatabase().close();
			Log.d("TEST", "****SUCCESSFULLY SAVE TO DB****\n\t===>" + highscore);
		} catch (Exception e) {
			Log.d("TEST", "****ERROR ON saveToDB****\n\t===>" + e.toString());
			e.printStackTrace();
		}
	}

	public String[] getColorHighScore() {
		int highscore = 0;
		String user = "";
		try {
			Cursor c = getReadableDatabase().rawQuery(
					"select MAX(high_score) as _highscore, name from "
							+ TB_color, null);
			if (c.moveToFirst()) {
				do {
					highscore = c.getInt(0);
					user = c.getString(c.getColumnIndex("name"));
					Log.d("TEST", "CURSOR:= " + c);
				} while (c.moveToNext());
			}
			c.close();
			getReadableDatabase().close();
			Log.d("TEST", "HIGHSCORE:= " + highscore);
			return new String[] { String.valueOf(highscore), user };
		} catch (Exception e) {
			getReadableDatabase().close();
			Log.d("TEST",
					"****ERROR ON getHighScore****\n\t===>" + e.toString());
			e.printStackTrace();
		}
		return null;
	}

	public String[] getNumberHighScore() {
		int highscore = 0;
		String user = "";
		try {
			Cursor c = getReadableDatabase().rawQuery(
					"select MAX(high_score) as _highscore, name from "
							+ TB_number, null);
			if (c.moveToFirst()) {
				do {
					highscore = c.getInt(0);
					user = c.getString(c.getColumnIndex("name"));
					Log.d("TEST", "CURSOR:= " + c);
				} while (c.moveToNext());
			}
			c.close();
			getReadableDatabase().close();
			Log.d("TEST", "HIGHSCORE:= " + highscore);
			return new String[] { String.valueOf(highscore), user };
		} catch (Exception e) {
			getReadableDatabase().close();
			Log.d("TEST",
					"****ERROR ON getHighScore****\n\t===>" + e.toString());
			e.printStackTrace();
		}
		return null;
	}

	public String[] getLetterHighScore() {
		int highscore = 0;
		String user = "";
		try {
			Cursor c = getReadableDatabase().rawQuery(
					"select MAX(high_score) as _highscore, name from " + TB_letter,
					null);
			if (c.moveToFirst()) {
				do {
					highscore = c.getInt(0);
					user = c.getString(c.getColumnIndex("name"));
					Log.d("TEST", "CURSOR:= " + c);
				} while (c.moveToNext());
			}
			c.close();
			getReadableDatabase().close();
			Log.d("TEST", "HIGHSCORE:= " + highscore);
			return new String[]{String.valueOf(highscore), user};
		} catch (Exception e) {
			getReadableDatabase().close();
			Log.d("TEST",
					"****ERROR ON getHighScore****\n\t===>" + e.toString());
			e.printStackTrace();
		}
		return null;
	}

}
