package org.android.sublearning;

import org.android.utilities.HighScoreDB;
import org.android.utilities.ToggleImageView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class highscore_activity extends Activity {

	ToggleImageView toggler = new ToggleImageView(this);
	HighScoreDB myDB = new HighScoreDB(this);

	ImageView mBtnHighscoreABC;
	ImageView mBtnHighscoreNumber;
	ImageView mBtnHighscoreColor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscore_activity);
		INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivity(new Intent(highscore_activity.this, quiz_activity.class));
		highscore_activity.this.finish();
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}

	private void INIT() {
		HighScore mHighScoreHandler = new HighScore();

		mBtnHighscoreABC = (ImageView) findViewById(R.id.img_highscore_abc);
		toggler.setImageResource(mBtnHighscoreABC,
				R.drawable.abc_highscore_unsel, R.drawable.abc_highscore_sel,
				mHighScoreHandler);

		mBtnHighscoreNumber = (ImageView) findViewById(R.id.img_highscore_number);
		toggler.setImageResource(mBtnHighscoreNumber,
				R.drawable.number_highscore_unsel,
				R.drawable.number_highscore_sel, mHighScoreHandler);

		mBtnHighscoreColor = (ImageView) findViewById(R.id.img_highscore_color);
		toggler.setImageResource(mBtnHighscoreColor,
				R.drawable.color_highscore_unsel,
				R.drawable.color_highscore_sel, mHighScoreHandler);
	}

	public class HighScore implements OnClickListener {

		@Override
		public void onClick(View v) {

			if (v == mBtnHighscoreABC) {
				Log.d("TEST", "LETTER HIGHSCORE:= " + myDB.getLetterHighScore());
				String[] high_score = myDB.getLetterHighScore();
				Intent i = new Intent(highscore_activity.this,
						high_score_view_activity.class);
				i.putExtra("title", "High Score is");
				i.putExtra("score", Integer.valueOf(high_score[0]));
				i.putExtra("user", high_score[1]);
				startActivity(i);
				highscore_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (v == mBtnHighscoreNumber) {
				Log.d("TEST", "NUMBER HIGHSCORE:= " + myDB.getNumberHighScore());
				String[] high_score = myDB.getNumberHighScore();
				Intent i = new Intent(highscore_activity.this,
						high_score_view_activity.class);
				i.putExtra("title", "High Score is");
				i.putExtra("score", Integer.valueOf(high_score[0]));
				i.putExtra("user", high_score[1]);
				startActivity(i);
				highscore_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (v == mBtnHighscoreColor) {
				Log.d("TEST", "COLOR HIGHSCORE:= " + myDB.getColorHighScore());
				String[] high_score = myDB.getColorHighScore();
				Intent i = new Intent(highscore_activity.this,
						high_score_view_activity.class);
				i.putExtra("title", "High Score is");
				i.putExtra("score", Integer.valueOf(high_score[0]));
				i.putExtra("user", high_score[1]);
				startActivity(i);
				highscore_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			}
		}

	}

}
