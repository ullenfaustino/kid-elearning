package org.android.model;


public class ModelChoices_D {

	private String D;

	public ModelChoices_D(String d) {
		setD(d);
	}

	public String getD() {
		return D;
	}

	public void setD(String d) {
		D = d;
	}

}
