package org.android.model;

public class ModelNumbers {

	private String NumberBG, NumberBtnSel, NumberBtnUnsel;

	public ModelNumbers(String numberBg, String numberBtnSel,
			String numberBtnUnsel) {
		setNumberBG(numberBg);
		setNumberBtnSel(numberBtnSel);
		setNumberBtnUnsel(numberBtnUnsel);
	}

	public String getNumberBG() {
		return NumberBG;
	}

	public void setNumberBG(String numberBG) {
		NumberBG = numberBG;
	}

	public String getNumberBtnSel() {
		return NumberBtnSel;
	}

	public void setNumberBtnSel(String numberBtnSel) {
		NumberBtnSel = numberBtnSel;
	}

	public String getNumberBtnUnsel() {
		return NumberBtnUnsel;
	}

	public void setNumberBtnUnsel(String numberBtnUnsel) {
		NumberBtnUnsel = numberBtnUnsel;
	}

}
