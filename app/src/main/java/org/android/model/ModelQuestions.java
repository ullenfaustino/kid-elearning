package org.android.model;


public class ModelQuestions {

	private String mQuestion;
	
	public ModelQuestions(String question) {
		setQuestion(question);
	}

	public String getQuestion() {
		return mQuestion;
	}

	public void setQuestion(String mQuestion) {
		this.mQuestion = mQuestion;
	}
	
}
