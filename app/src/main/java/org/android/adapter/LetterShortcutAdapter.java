package org.android.adapter;

import java.util.ArrayList;

import org.android.model.ModelLetters;
import org.android.sublearning.R;
import org.android.utilities.ToggleImageView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class LetterShortcutAdapter extends BaseAdapter {

	LayoutInflater inflater;
	ArrayList<ModelLetters> arr_letters;
	ToggleImageView toggler;

	public LetterShortcutAdapter(Context ctx, ArrayList<ModelLetters> data) {
		this.inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.arr_letters = data;
		this.toggler = new ToggleImageView(ctx);
	}

	@Override
	public int getCount() {
		return arr_letters.size();
	}

	@Override
	public Object getItem(int pos) {
		return pos;
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View vw = view;

		if (vw == null) {
			vw = inflater.inflate(R.layout.grid_button_image, parent, false);
		}

		ImageView img = (ImageView) vw.findViewById(R.id.grid_image_btn);

		ModelLetters mModel = arr_letters.get(position);
		String name = mModel.getLetterBG().replace(".png", "");

		String name_unsel = "Letters/letters_shortcut/" + name + "_unsel.png";
		String name_sel = "Letters/letters_shortcut/" + name + "_sel.png";
		
		toggler.setImageFromAssets(img, name_unsel, name_sel);
		return vw;
	}

}
